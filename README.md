# TGCA_Analysis


In this R file you can have a look at a TGCA analysis that I did thanks to a tutorial : https://www.costalab.org/wp-content/uploads/2020/11/R_class_D3.html

The main steps that we do with TCGA data are : 

RNASeq Normalization

Classification

Survival analysis

Gene expression and Survival

If you don't want to run the first commands to create tcga_data you can download the file here and open it with R.

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

July 2023
